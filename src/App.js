import React from 'react';
import { Route, Switch} from 'react-router';
import './App.css';
import HomePage from "./pages/Home";
import FilmPage from "./pages/Film";
import SearchPage from "./pages/Search";
import AuthPage from "./pages/Auth";

const App = () =>{
    return <section>
        <Switch>
            <Route exact path="/" component={HomePage}/>
            <Route path="/film/:id" component={FilmPage}/>
            <Route path="/search" component={SearchPage}/>
            <Route path="/auth" component={AuthPage}/>
        </Switch>
    </section>
};

export default App;
