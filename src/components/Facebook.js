import React, { Component } from 'react';
import  FacebookLogin from 'react-facebook-login';
import {Redirect} from "react-router";

export default class Facebook extends Component {
    state = {
        isLoggedIn: localStorage.getItem('isLoggedIn'),
        userID: '',
        name: '',
        picture: ''
    }

    responseFacebook = response => {
        this.setState({
            isLoggedIn: true,
            name: response.name,
            picture: response.picture.data.url
        });
        localStorage.setItem('isLoggedIn', true);
        localStorage.setItem('FBname', response.name);
        document.location.reload(true);
    }

    componentClicked = () => console.log("click");

    render() {
        let fbContent;
        if(this.state.isLoggedIn){
            fbContent = (
                    <Redirect to="/" />
            );
        }else{
            fbContent = (<FacebookLogin
                    appId="298710720920947"
                    autoLoad={false}
                    fields="name,picture"
                    onClick={this.componentClicked}
                    callback={this.responseFacebook} />);
        }
        return (
            <div>
                {fbContent}
            </div>
        )
    }
}