import React, {Component} from "react";

class Components extends Component {

    constructor(props){
        super(props)
        this.state = {localStorage: ''}
    }

    ExitButton() {
        localStorage.setItem("myData", '');
        this.setState({
            localStorage: ''
        })
    }

    getData(){
        return localStorage.getItem("myData")
    }

    updateDataLocalStorage(){
        this.setState({
            localStorage: localStorage.getItem("myData")
        })
    }
};
export default Components;