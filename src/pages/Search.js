import React, { Component } from 'react';
import '../index.css';
import '../App.css';
import MenuPage from "../pages/Menu";
import HutPage from "../pages/Hut";
import {Redirect} from "react-router";

const img_url = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/';
class Search extends Component {
    constructor(props){
        super(props);
        this.state = {isLoggedIn: localStorage.getItem('isLoggedIn')};
        this.setState({
            text: "123",
            text2: "321"
        });

        this.testC = this.testC.bind(this);
        this.testB = this.testB.bind(this);
        this.testS = this.testS.bind(this);
    }

    testC(e){
        this.setState({
            text: e.target.value
        })
    }

    testB(e){
        this.setState({
            text2: e.target.value
        })
    }

    testS(e){
        fetch('https://api.themoviedb.org/3/discover/movie?primary_release_date.gte=' + this.state.text + '&primary_release_date.lte=' + this.state.text2 + '&api_key=52f74b67ef758d7122a5bfd5c96301ae')
            .then(d => d.json())
            .then(data => {
                this.setState({
                    githubData: data.results
                })
            });
    }

    render() {
        if (!this.state.isLoggedIn) return <Redirect to="/auth" />
        if (!this.state.githubData) return <div><HutPage /><MenuPage />
            <div>
                <input type="date" onChange={this.testC} min="2012-05-29" max="2018-12-31" />
                <span> - </span>
                <input type="date" onChange={this.testB} min="2012-05-29" max="2018-12-31" />
                <button onClick={this.testS} type="submit">Поиск</button>
            </div>
            <p> Loading.....</p></div>
        return (
            <div>
                <HutPage />
                <div className="AppBody">
                    <MenuPage />
                    <div>
                        <input type="date" onChange={this.testC} value={this.state.text} min="2012-05-29" max="2018-12-31" />
                        <span> - </span>
                        <input type="date" onChange={this.testB} value={this.state.text2} min="2012-05-29" max="2018-12-31" />
                        <button onClick={this.testS} type="submit">Поиск</button>
                    </div>
                    <br />
                </div>
                <div class="body_div_block">
                    {this.state.githubData.map(res =>
                        <a href={"film/" + res.id} target="_blank">
                            <div class="body_div_item" id={res.id}>
                                <img src={img_url + res.poster_path} alt="" /><br />
                                <div>
                                    <h1>{res.title}</h1>
                                </div>

                            </div></a>
                    )}
                </div>
            </div>
        )
    }
}

export default Search;