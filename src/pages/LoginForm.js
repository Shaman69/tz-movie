import React, { Component } from 'react';
import {Redirect} from "react-router";
import "./Components";
import Facebook from "../components/Facebook";

class LoginForm extends Component {
    constructor(props){
        super(props)
        this.state = {  login: '',
                        pass: '',
                        isLoggedIn: localStorage.getItem('isLoggedIn')
        }

        this.StateLogin = this.StateLogin.bind(this);
        this.StatePass = this.StatePass.bind(this);
        this.Auth = this.Auth.bind(this);
    }

    StateLogin(e){
        this.setState({
            login: e.target.value
        })
    }

    StatePass(e){
        this.setState({
            pass: e.target.value
        })
    }

    Auth(){
        if( this.state.login == "Admin" && this.state.pass == "admin") {
            localStorage.setItem("myData", '2');
            this.setState({
                isLoggedIn: true
            });
            localStorage.setItem('FBname', this.state.login);
            localStorage.setItem('isLoggedIn', true);
        }
    }

    render() {
        let loginform;
        if(this.state.isLoggedIn){
            loginform = (
            <Redirect to="/" />
            );
        }else {
            loginform = (
                <div>
                    <div className="AppBody">
                        <br/>
                        <input onChange={this.StateLogin} placeholder="Login...."/>
                        <input onChange={this.StatePass} type="password"/>
                        <button onClick={this.Auth}>Login</button>
                        <span><Facebook/></span>
                    </div>
                </div>
            );
        }

        return (
            <div>
                {loginform}
            </div>
            )
    }
}

export default LoginForm;