import React, { Component } from 'react';
import { Redirect } from 'react-router';
import '../index.css';
import logo from '../logo.svg';
import '../App.css';
import MenuPage from "../pages/Menu";
import HutPage from "./Hut";

const img_url = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/';
class Home extends Component {
	constructor(props){
		super(props);
		this.state = {d1: '2018-01-01', d2: '2018-12-30', isLoggedIn: localStorage.getItem('isLoggedIn')};
	}

	componentDidMount(){
        const value = this.state.d1;
        const value2 = this.state.d2;
		fetch('https://api.themoviedb.org/3/discover/movie?primary_release_date.gte=' + value + '&primary_release_date.lte=' + value2 + '&api_key=52f74b67ef758d7122a5bfd5c96301ae')
			.then(d => d.json())
			.then(data => {
				this.setState({
					githubData: data.results
				})
			});
	}

	render() {
		if (!this.state.isLoggedIn) return <Redirect to="/auth" />
		if (!this.state.githubData) return <p>Loading .....[{this.state.isLoggedIn}]</p>
		return (
			<div>
                <HutPage />
        		<div className="AppBody">
          			<MenuPage />
        		</div>
        		<div class="body_div_block">
                        {this.state.githubData.map(res =>
							<a href={"film/" + res.id}>
                            <div class="body_div_item" id={res.id}>
								<img src={img_url + res.poster_path} alt="" /><br />
                                <div>
                                    <h1>{res.title}</h1>
                                </div>

							</div></a>
                        )}
        		</div>
      		</div>
		)
	}
}

export default Home;