import React, { Component } from 'react';
import '../index.css';
import logo from '../logo.svg';
import '../App.css';
import MenuPage from "../pages/Menu";
import HutPage from "./Hut";
import {Redirect} from "react-router";

const img_url = "https://image.tmdb.org/t/p/w185_and_h278_bestv2/";

class Film extends Component {
    release_date;
    poster_path;
    overview;
    constructor(props){
        super(props);
        this.state = {isLoggedIn: localStorage.getItem('isLoggedIn')}
    }

    componentDidMount(){
        const value = this.props.match.params.id;
        fetch('https://api.themoviedb.org/3/movie/' + value + '?api_key=52f74b67ef758d7122a5bfd5c96301ae')
            .then(d => d.json())
            .then(data => {
                this.setState({
                    githubData: data,
                    test: value
                })
            });
    }
    render() {
        if (!this.state.isLoggedIn) return <Redirect to="/auth" />
        if (!this.state.githubData) return <p>Loading .....</p>
        return (
            <div>
                <HutPage />
                <div className="AppBody">
                    <MenuPage /><br />
                </div>
                <div class="body_div_blockf">
                        <div class="body_div_itemf">
                            <img src={img_url + this.state.githubData.poster_path} alt=""/><br />
                            <div>
                                <h1>{this.state.githubData.title}</h1><br />
                                <a>{this.state.githubData.overview}</a><br /><br />
                                <a className="App-title">Дата релиза: {this.state.githubData.release_date}</a>
                            </div>

                        </div>

                </div>
            </div>
        )
    }
}

export default Film;