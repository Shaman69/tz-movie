import logo from "../logo.svg";
import React, {Component} from "react";
import Film from "./Film";
import {Redirect} from "react-router";

class Hut extends Component {

    constructor(props){
        super(props)
        this.state = {}
    }

    ExitButton() {
        localStorage.removeItem('isLoggedIn');
        localStorage.removeItem('FBname');
        document.location.reload(true);
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">TZ</h1>
                    <button onClick={this.ExitButton}>выход [ {localStorage.getItem('FBname')} ]</button>
                </header>
            </div>
        )
    }
};
export default Hut;